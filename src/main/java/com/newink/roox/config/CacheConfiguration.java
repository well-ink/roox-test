package com.newink.roox.config;

import com.newink.roox.domain.Customer;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.ehcache.jsr107.Eh107Configuration;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
@EnableCaching
@AutoConfigureBefore(value = {WebConfigurer.class, DatabaseConfiguration.class})
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(ApplicationProperties applicationProperties) {
        ApplicationProperties.Cache.Ehcache ehcache =
            applicationProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(Expirations.timeToLiveExpiration(Duration.of(ehcache.getTimeToLiveSeconds(), TimeUnit.SECONDS)))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(com.newink.roox.repository.CustomerRepository.CUSTOMERS_BY_LOGIN_CACHE, jcacheConfiguration);
            cm.createCache(com.newink.roox.repository.CustomerRepository.CUSTOMERS_BY_EMAIL_CACHE, jcacheConfiguration);
            cm.createCache(Customer.class.getName(), jcacheConfiguration);
            cm.createCache(com.newink.roox.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(Customer.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(Customer.class.getName() + ".partnerMappings", jcacheConfiguration);
            cm.createCache(com.newink.roox.domain.PartnerMapping.class.getName(), jcacheConfiguration);
        };
    }

}
