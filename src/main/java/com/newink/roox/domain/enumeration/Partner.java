package com.newink.roox.domain.enumeration;

/**
 * The Partner enumeration.
 */
public enum Partner {
    FACEBOOK, INSTAGRAM, TWITTER
}
