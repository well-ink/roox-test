package com.newink.roox.repository;

import com.newink.roox.domain.Customer;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data JPA repository for the Customer entity.
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    String CUSTOMERS_BY_LOGIN_CACHE = "customersByLogin";

    String CUSTOMERS_BY_EMAIL_CACHE = "customersByEmail";

    Optional<Customer> findOneByLogin(String login);

    Optional<Customer> findOneById(Long id);

    @EntityGraph(attributePaths = {"authorities", "partnerMappings"})
    Optional<Customer> findOneWithAuthoritiesById(Long id);

    @EntityGraph(attributePaths = {"authorities", "partnerMappings"})
    @Cacheable(cacheNames = CUSTOMERS_BY_LOGIN_CACHE)
    Optional<Customer> findOneWithAuthoritiesByLogin(String login);

    @EntityGraph(attributePaths = {"authorities", "partnerMappings"})
    @Cacheable(cacheNames = CUSTOMERS_BY_EMAIL_CACHE)
    Optional<Customer> findOneWithAuthoritiesByEmail(String email);

    Page<Customer> findAllByLoginNot(Pageable pageable, String login);
}
