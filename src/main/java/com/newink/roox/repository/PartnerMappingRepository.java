package com.newink.roox.repository;

import com.newink.roox.domain.Customer;
import com.newink.roox.domain.PartnerMapping;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the PartnerMapping entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PartnerMappingRepository extends JpaRepository<PartnerMapping, Long> {

    @Query("select partner_mapping from PartnerMapping partner_mapping where partner_mapping.customer.login = ?#{principal.username}")
    Page<PartnerMapping> findByCurrentCustomer(Pageable pageable);

    Page<PartnerMapping> findByCustomer(Customer customer, Pageable pageable);

    @Query("select partner_mapping from PartnerMapping partner_mapping where partner_mapping.customer.login = ?#{principal.username} and partner_mapping.id = :id")
    PartnerMapping findByIdAndCurrentCustomer(@Param("id") Long id);
}
