package com.newink.roox.security;

import org.springframework.security.core.AuthenticationException;

/**
 * This exception is thrown in case of a not activated customer trying to authenticate.
 */
public class CustomerNotActivatedException extends AuthenticationException {

    private static final long serialVersionUID = 1L;

    public CustomerNotActivatedException(String message) {
        super(message);
    }

    public CustomerNotActivatedException(String message, Throwable t) {
        super(message, t);
    }
}
