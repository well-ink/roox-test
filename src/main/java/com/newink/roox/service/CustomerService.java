package com.newink.roox.service;

import com.newink.roox.config.Constants;
import com.newink.roox.domain.Authority;
import com.newink.roox.domain.Customer;
import com.newink.roox.repository.AuthorityRepository;
import com.newink.roox.repository.CustomerRepository;
import com.newink.roox.security.SecurityUtils;
import com.newink.roox.service.dto.CustomerDTO;
import com.newink.roox.service.mapper.CustomerMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service class for managing customers.
 */
@Service
@Transactional
public class CustomerService {

    private final Logger log = LoggerFactory.getLogger(CustomerService.class);

    private final CustomerRepository customerRepository;

    private final AuthorityRepository authorityRepository;

    private final CustomerMapper customerMapper;

    public CustomerService(CustomerRepository customerRepository, AuthorityRepository authorityRepository, CustomerMapper customerMapper) {
        this.customerRepository = customerRepository;
        this.authorityRepository = authorityRepository;
        this.customerMapper = customerMapper;
    }

    @Transactional(readOnly = true)
    public Page<CustomerDTO> getAllManagedCustomers(Pageable pageable) {
        return customerRepository.findAllByLoginNot(pageable, Constants.ANONYMOUS_USER).map(customerMapper::customerToCustomerDTO);
    }

    @Transactional(readOnly = true)
    public Optional<Customer> getCustomerWithAuthoritiesByLogin(String login) {
        return customerRepository.findOneWithAuthoritiesByLogin(login);
    }

    @Transactional(readOnly = true)
    public Optional<Customer> getCustomerWithAuthorities(Long id) {
        return customerRepository.findOneWithAuthoritiesById(id);
    }

    @Transactional(readOnly = true)
    public Optional<Customer> getCustomerWithAuthorities() {
        return SecurityUtils.getCurrentCustomerLogin().flatMap(customerRepository::findOneWithAuthoritiesByLogin);
    }

    @Transactional(readOnly = true)
    public Optional<Customer> getCustomer(Long id) {
        return customerRepository.findOneById(id);
    }

    @Transactional(readOnly = true)
    public Optional<Customer> getCurrentCustomer() {
        return SecurityUtils.getCurrentCustomerLogin().flatMap(customerRepository::findOneByLogin);
    }

    /**
     * @return a list of all the authorities
     */
    public List<String> getAuthorities() {
        return authorityRepository.findAll().stream().map(Authority::getName).collect(Collectors.toList());
    }

}
