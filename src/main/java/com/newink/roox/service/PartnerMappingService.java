package com.newink.roox.service;

import com.newink.roox.domain.Customer;
import com.newink.roox.domain.PartnerMapping;
import com.newink.roox.repository.PartnerMappingRepository;
import com.newink.roox.service.mapper.CustomerMapper;
import com.newink.roox.web.rest.errors.UserNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service class for managing partner mappings for customers.
 */
@Service(value = "partnerMappingService")
@Transactional
public class PartnerMappingService {

    private final Logger log = LoggerFactory.getLogger(PartnerMappingService.class);

    private final CustomerService customerService;

    private final PartnerMappingRepository partnerMappingRepository;

    private final CustomerMapper customerMapper;

    public PartnerMappingService(CustomerService customerService, PartnerMappingRepository partnerMappingRepository, CustomerMapper customerMapper) {
        this.customerService = customerService;
        this.partnerMappingRepository = partnerMappingRepository;
        this.customerMapper = customerMapper;
    }

    public Page<PartnerMapping> findAllCustomerMappings(Pageable pageable, Long customerId) {
        log.debug("Request to get request mappings for customer with id: {}", customerId);
        return partnerMappingRepository.findByCustomer(customerMapper.customerFromId(customerId), pageable);
    }

    public PartnerMapping save(PartnerMapping partnerMapping) {
        log.debug("Saving new partner mapping: {}", partnerMapping);
        return partnerMappingRepository.save(partnerMapping);
    }

    public PartnerMapping save(PartnerMapping partnerMapping, Long customerId) {
        log.debug("Saving new partner mapping for customer with id {}: {}", customerId, partnerMapping);
        Customer customer = customerService.getCustomer(customerId).orElseThrow(UserNotFoundException::new);
        return save(partnerMapping, customer);
    }

    public PartnerMapping saveWithCurrentCustomer(PartnerMapping partnerMapping) {
        log.debug("Saving new partner mapping for current user: {}", partnerMapping);
        Customer currentCustomer = customerService.getCurrentCustomer().orElseThrow(UserNotFoundException::new);
        return save(partnerMapping, currentCustomer);
    }

    @Transactional(readOnly = true)
    public Page<PartnerMapping> findAll(Pageable pageable) {
        log.debug("Request to get page of partner mappings: {}", pageable);
        return partnerMappingRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public Page<PartnerMapping> findAllForCurrentCustomer(Pageable pageable) {
        log.debug("Request to get page of partner mappings for current customer: {}", pageable);
        return partnerMappingRepository.findByCurrentCustomer(pageable);
    }

    @Transactional(readOnly = true)
    public PartnerMapping findOne(Long id) {
        log.debug("Request to get partner mapping with id: {}", id);
        return partnerMappingRepository.findOne(id);
    }

    public void delete(Long id) {
        log.debug("Request to delete partner mapping with id: {}", id);
        partnerMappingRepository.delete(id);
    }

    private PartnerMapping save(PartnerMapping partnerMapping, Customer customer) {
        partnerMapping.setCustomer(customer);
        return partnerMappingRepository.save(partnerMapping);
    }

    /**
     * Checks if current user is owner of edited partner mapping
     *
     * @param mappingId id of checked partner mapping
     */
    public boolean checkIfCustomerHasAccessToMapping(Long mappingId) {
        return customerService
            .getCurrentCustomer()
            .orElse(new Customer())
            .getPartnerMappings()
            .stream()
            .anyMatch(partnerMapping -> partnerMapping.getId().equals(mappingId));
    }
}
