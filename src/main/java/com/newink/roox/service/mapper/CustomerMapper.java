package com.newink.roox.service.mapper;

import com.newink.roox.domain.Authority;
import com.newink.roox.domain.Customer;
import com.newink.roox.service.dto.CustomerDTO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Mapper for the entity Customer and its DTO called CustomerDTO.
 * <p>
 * Normal mappers are generated using MapStruct, this one is hand-coded as MapStruct
 * support is still in beta, and requires a manual step with an IDE.
 */
@Service
public class CustomerMapper {

    public CustomerDTO customerToCustomerDTO(Customer customer) {
        return new CustomerDTO(customer);
    }

    public List<CustomerDTO> customersToCustomerDTOs(List<Customer> customers) {
        return customers.stream()
            .filter(Objects::nonNull)
            .map(this::customerToCustomerDTO)
            .collect(Collectors.toList());
    }

    public Customer customerDTOtoCustomer(CustomerDTO customerDTO) {
        if (customerDTO == null) {
            return null;
        } else {
            Customer customer = new Customer();
            customer.setId(customerDTO.getId());
            customer.setLogin(customerDTO.getLogin());
            customer.setFirstName(customerDTO.getFirstName());
            customer.setLastName(customerDTO.getLastName());
            customer.setEmail(customerDTO.getEmail());
            customer.setActivated(customerDTO.isActivated());
            Set<Authority> authorities = this.authoritiesFromStrings(customerDTO.getAuthorities());
            if (authorities != null) {
                customer.setAuthorities(authorities);
            }
            return customer;
        }
    }

    public List<Customer> customerDTOsToCustomers(List<CustomerDTO> customerDTOS) {
        return customerDTOS.stream()
            .filter(Objects::nonNull)
            .map(this::customerDTOtoCustomer)
            .collect(Collectors.toList());
    }

    public Customer customerFromId(Long id) {
        if (id == null) {
            return null;
        }
        Customer customer = new Customer();
        customer.setId(id);
        return customer;
    }

    public Set<Authority> authoritiesFromStrings(Set<String> strings) {
        return strings.stream().map(string -> {
            Authority auth = new Authority();
            auth.setName(string);
            return auth;
        }).collect(Collectors.toSet());
    }
}
