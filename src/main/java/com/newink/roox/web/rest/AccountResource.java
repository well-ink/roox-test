package com.newink.roox.web.rest;

import com.newink.roox.service.CustomerService;
import com.newink.roox.service.dto.CustomerDTO;
import com.newink.roox.service.mapper.CustomerMapper;
import com.newink.roox.web.rest.errors.InternalServerErrorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * REST controller for managing the current customer's account.
 */
@RestController
@RequestMapping("/api")
public class AccountResource {

    private final Logger log = LoggerFactory.getLogger(AccountResource.class);

    private final CustomerService customerService;

    private final CustomerMapper customerMapper;

    public AccountResource(CustomerService customerService, CustomerMapper customerMapper) {
        this.customerService = customerService;
        this.customerMapper = customerMapper;
    }


    /**
     * GET  /authenticate : check if the customer is authenticated, and return its login.
     *
     * @param request the HTTP request
     * @return the login if the customer is authenticated
     */
    @GetMapping("/authenticate")
    public String isAuthenticated(HttpServletRequest request) {
        log.debug("REST request to check if the current customer is authenticated");
        return request.getRemoteUser();
    }

    /**
     * GET  /account : get the current customer.
     *
     * @return the current customer
     * @throws RuntimeException 500 (Internal Server Error) if the customer couldn't be returned
     */
    @Secured("ROLE_USER")
    @GetMapping("/account")
    public CustomerDTO getAccount() {
        return customerService.getCustomerWithAuthorities()
            .map(customerMapper::customerToCustomerDTO)
            .orElseThrow(() -> new InternalServerErrorException("Customer could not be found"));
    }
}
