package com.newink.roox.web.rest;

import com.newink.roox.security.AuthoritiesConstants;
import com.newink.roox.service.CustomerService;
import com.newink.roox.service.dto.CustomerDTO;
import com.newink.roox.service.mapper.CustomerMapper;
import com.newink.roox.web.rest.util.PaginationUtil;
import com.newink.roox.web.rest.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * REST controller for managing customers.
 */
@RestController
@RequestMapping("/api")
public class CustomerResource {

    private final Logger log = LoggerFactory.getLogger(CustomerResource.class);

    private final CustomerService customerService;

    private final CustomerMapper customerMapper;

    public CustomerResource(CustomerService customerService, CustomerMapper customerMapper) {
        this.customerService = customerService;
        this.customerMapper = customerMapper;
    }

    /**
     * GET /customers : get all customers.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and with body all customers
     */
    @Secured("ROLE_ADMIN")
    @GetMapping("/customers")
    public ResponseEntity<List<CustomerDTO>> getAllCustomers(Pageable pageable) {
        final Page<CustomerDTO> page = customerService.getAllManagedCustomers(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/customers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * @return a string list of the all of the roles
     */
    @GetMapping("/customers/authorities")
    @Secured(AuthoritiesConstants.ADMIN)
    public List<String> getAuthorities() {
        return customerService.getAuthorities();
    }


    /**
     * GET /customers/:id : get the "id" customer.
     *
     * @param id the id of the customer to find
     * @return the ResponseEntity with status 200 (OK) and with body the "id" customer, or with status 404 (Not Found)
     */
    @Secured("ROLE_ADMIN")
    @GetMapping("/customers/{id}")
    public ResponseEntity<CustomerDTO> getCustomer(@PathVariable Long id) {
        log.debug("REST request to get Customer : {}", id);
        return ResponseUtil.wrapOrNotFound(
            customerService.getCustomerWithAuthorities(id)
                .map(customerMapper::customerToCustomerDTO));
    }
}
