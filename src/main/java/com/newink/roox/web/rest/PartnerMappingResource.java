package com.newink.roox.web.rest;

import com.newink.roox.domain.PartnerMapping;
import com.newink.roox.service.PartnerMappingService;
import com.newink.roox.web.rest.errors.BadRequestAlertException;
import com.newink.roox.web.rest.util.HeaderUtil;
import com.newink.roox.web.rest.util.PaginationUtil;
import com.newink.roox.web.rest.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PartnerMapping.
 */
@RestController
@RequestMapping("/api")
public class PartnerMappingResource {

    private final Logger log = LoggerFactory.getLogger(PartnerMappingResource.class);

    private static final String ENTITY_NAME = "partnerMapping";

    private final PartnerMappingService partnerMappingService;

    public PartnerMappingResource(PartnerMappingService partnerMappingService) {
        this.partnerMappingService = partnerMappingService;
    }

    /**
     * POST  /customers/{id}/partner-mappings : Create a new partnerMapping owned by customer with specified id.
     *
     * @param partnerMapping the partnerMapping to create
     * @param customerId             of owner customer
     * @return the ResponseEntity with status 201 (Created) and with body the new partnerMapping, or with status 400 (Bad Request) if the partnerMapping has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/customers/{customerId:[0-9]+}/partner-mappings")
    @PreAuthorize("hasRole('ROLE_USER') and principal.id == #customerId")
    public ResponseEntity<PartnerMapping> createPartnerMappingForCustomer(@Valid @RequestBody PartnerMapping partnerMapping, @PathVariable Long customerId) throws URISyntaxException {
        log.debug("REST request to save PartnerMapping : {}", partnerMapping);
        if (partnerMapping.getId() != null) {
            throw new BadRequestAlertException("A new partnerMapping cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PartnerMapping result = partnerMappingService.save(partnerMapping, customerId);
        return ResponseEntity.created(new URI("/api/partner-mappings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * GET  /customers/{id}/partner-mappings : get all the partnerMappings of customer with specified id.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of partnerMappings in body
     */
    @GetMapping("/customers/{customerId:[0-9]+}/partner-mappings")
    @PreAuthorize("hasRole('ROLE_USER') and principal.id == #customerId")
    public ResponseEntity<List<PartnerMapping>> getAllPartnerMappings(Pageable pageable, @PathVariable Long customerId) {
        log.debug("REST request to get a page of PartnerMappings of customer with id: {}", customerId);
        Page<PartnerMapping> page = partnerMappingService.findAllCustomerMappings(pageable, customerId);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/customer/" + customerId + "/partner-mappings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * POST  /customers/@me/partner-mappings : Create a new partnerMapping owned by customer which is identified by current session.
     *
     * @param partnerMapping the partnerMapping to create
     * @return the ResponseEntity with status 201 (Created) and with body the new partnerMapping, or with status 400 (Bad Request) if the partnerMapping has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @Secured("ROLE_USER")
    @PostMapping("/customers/@me/partner-mappings")
    public ResponseEntity<PartnerMapping> createPartnerMappingForCurrentCustomer(@Valid @RequestBody PartnerMapping partnerMapping) throws URISyntaxException {
        log.debug("REST request to save PartnerMapping: {}", partnerMapping);
        if (partnerMapping.getId() != null) {
            throw new BadRequestAlertException("A new partnerMapping cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PartnerMapping result = partnerMappingService.saveWithCurrentCustomer(partnerMapping);
        return ResponseEntity.created(new URI("/api/partner-mappings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * GET  /customers/@me/partner-mappings : get all the partnerMappings of customer which is identified by current session.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of partnerMappings in body
     */
    @Secured("ROLE_USER")
    @GetMapping("/customers/@me/partner-mappings")
    public ResponseEntity<List<PartnerMapping>> getAllCurrentCustomerPartnerMappings(Pageable pageable) {
        log.debug("REST request to get a page of PartnerMappings of current customer");
        Page<PartnerMapping> page = partnerMappingService.findAllForCurrentCustomer(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/customer/@me/partner-mappings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * PUT  /partner-mappings : Updates an existing partnerMapping.
     *
     * @param partnerMapping the partnerMapping to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated partnerMapping,
     * or with status 400 (Bad Request) if the partnerMapping is not valid,
     * or with status 500 (Internal Server Error) if the partnerMapping couldn't be updated
     */
    @PutMapping("/customers/@me/partner-mappings/")
    @PreAuthorize("hasRole('ROLE_USER') and @partnerMappingService.checkIfCustomerHasAccessToMapping(#partnerMapping.id)")
    public ResponseEntity<PartnerMapping> updatePartnerMappingForCurrentUser(@Valid @RequestBody PartnerMapping partnerMapping) throws URISyntaxException {
        log.debug("REST request to update PartnerMapping : {}", partnerMapping);
        if (partnerMapping.getId() == null) {
            return createPartnerMappingForCurrentCustomer(partnerMapping);
        }
        PartnerMapping result = partnerMappingService.saveWithCurrentCustomer(partnerMapping);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, partnerMapping.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /partner-mappings : Updates an existing partnerMapping of current user
     *
     * @param partnerMapping the partnerMapping to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated partnerMapping,
     * or with status 400 (Bad Request) if the partnerMapping is not valid,
     * or with status 500 (Internal Server Error) if the partnerMapping couldn't be updated
     */
    @PutMapping("/customers/{customerId:[0-9]+}/partner-mappings/")
    @PreAuthorize("hasRole('ROLE_USER') and principal.id == #customerId")
    public ResponseEntity<PartnerMapping> updatePartnerMapping(@Valid @RequestBody PartnerMapping partnerMapping, @PathVariable Long customerId) throws URISyntaxException {
        log.debug("REST request to update PartnerMapping : {}", partnerMapping);
        if (partnerMapping.getId() == null) {
            return createPartnerMappingForCustomer(partnerMapping, customerId);
        }
        PartnerMapping result = partnerMappingService.save(partnerMapping);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, partnerMapping.getId().toString()))
            .body(result);
    }

    /*
     * Customer-independent endpoints.
     * */

    /**
     * GET  /partner-mappings/:id : get the "id" partnerMapping.
     *
     * @param id the id of the partnerMapping to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the partnerMapping, or with status 404 (Not Found)
     */
    @GetMapping("/partner-mappings/{id}")
    @PreAuthorize("hasRole('ROLE_USER') and @partnerMappingService.checkIfCustomerHasAccessToMapping(#id)")
    public ResponseEntity<PartnerMapping> getPartnerMapping(@PathVariable Long id) {
        log.debug("REST request to get PartnerMapping : {}", id);
        PartnerMapping partnerMapping = partnerMappingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(partnerMapping));
    }

    /**
     * DELETE  /partner-mappings/:id : delete the "id" partnerMapping.
     *
     * @param id the id of the partnerMapping to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/partner-mappings/{id}")
    @PreAuthorize("hasRole('ROLE_USER') and @partnerMappingService.checkIfCustomerHasAccessToMapping(#id)")
    public ResponseEntity<Void> deletePartnerMapping(@PathVariable Long id) {
        log.debug("REST request to delete PartnerMapping : {}", id);
        partnerMappingService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
