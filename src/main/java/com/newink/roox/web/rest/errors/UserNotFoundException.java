package com.newink.roox.web.rest.errors;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class UserNotFoundException extends AbstractThrowableProblem {

    public UserNotFoundException() {
        super(ErrorConstants.EMAIL_NOT_FOUND_TYPE, "User with such identifier was not found", Status.BAD_REQUEST);
    }
}
