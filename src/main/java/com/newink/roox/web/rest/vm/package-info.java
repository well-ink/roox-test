/**
 * View Models used by Spring MVC REST controllers.
 */
package com.newink.roox.web.rest.vm;
