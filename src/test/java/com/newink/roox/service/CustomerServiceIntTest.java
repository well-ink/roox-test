package com.newink.roox.service;

import com.newink.roox.TestApp;
import com.newink.roox.config.Constants;
import com.newink.roox.domain.Customer;
import com.newink.roox.repository.CustomerRepository;
import com.newink.roox.service.dto.CustomerDTO;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test class for the UserResource REST controller.
 *
 * @see CustomerService
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApp.class)
@Transactional
public class CustomerServiceIntTest {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerService customerService;

    private Customer customer;

    @Before
    public void init() {
        customer = new Customer();
        customer.setLogin("johndoe");
        customer.setPassword(RandomStringUtils.random(60));
        customer.setActivated(true);
        customer.setEmail("johndoe@localhost");
        customer.setFirstName("john");
        customer.setLastName("doe");
    }

    @Test
    @Transactional
    public void assertThatAnonymousUserIsNotGet() {
        customer.setLogin(Constants.ANONYMOUS_USER);
        if (!customerRepository.findOneByLogin(Constants.ANONYMOUS_USER).isPresent()) {
            customerRepository.saveAndFlush(customer);
        }
        final PageRequest pageable = new PageRequest(0, (int) customerRepository.count());
        final Page<CustomerDTO> allManagedUsers = customerService.getAllManagedCustomers(pageable);
        assertThat(allManagedUsers.getContent().stream()
            .noneMatch(user -> Constants.ANONYMOUS_USER.equals(user.getLogin())))
            .isTrue();
    }
}
