package com.newink.roox.web.rest;

import com.newink.roox.TestApp;
import com.newink.roox.domain.Authority;
import com.newink.roox.domain.Customer;
import com.newink.roox.repository.CustomerRepository;
import com.newink.roox.security.AuthoritiesConstants;
import com.newink.roox.service.CustomerService;
import com.newink.roox.service.dto.CustomerDTO;
import com.newink.roox.service.mapper.CustomerMapper;
import com.newink.roox.web.rest.errors.ExceptionTranslator;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.CacheManager;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UserResource REST controller.
 *
 * @see CustomerResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApp.class)
public class CustomerResourceIntTest {

    private static final String DEFAULT_LOGIN = "admin";

    private static final Long DEFAULT_ID = 3L;

    private static final String DEFAULT_EMAIL = "johndoe@localhost";

    private static final String DEFAULT_FIRSTNAME = "john";

    private static final String DEFAULT_LASTNAME = "doe";

    private static final BigDecimal DEFAULT_BALANCE = BigDecimal.TEN;

    private static final String DEFAULT_LANGKEY = "en";

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CustomerMapper customerMapper;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private CacheManager cacheManager;

    private MockMvc restCustomerMockMvc;

    private Customer customer;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        cacheManager.getCache(CustomerRepository.CUSTOMERS_BY_LOGIN_CACHE).clear();
        cacheManager.getCache(CustomerRepository.CUSTOMERS_BY_EMAIL_CACHE).clear();
        CustomerResource customerResource = new CustomerResource(customerService, customerMapper);
        this.restCustomerMockMvc = MockMvcBuilders.standaloneSetup(customerResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter)
            .build();
    }

    /**
     * Create a Customer.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which has a required relationship to the Customer entity.
     */
    public static Customer createEntity(EntityManager em) {
        Customer customer = new Customer();
        customer.setId(DEFAULT_ID);
        customer.setLogin(DEFAULT_LOGIN);
        customer.setPassword(RandomStringUtils.random(60));
        customer.setActivated(true);
        customer.setEmail(RandomStringUtils.randomAlphabetic(5) + DEFAULT_EMAIL);
        customer.setFirstName(DEFAULT_FIRSTNAME);
        customer.setLastName(DEFAULT_LASTNAME);
        customer.setBalance(BigDecimal.TEN);
        return customer;
    }

    @Before
    public void initTest() {
        customer = createEntity(em);
        customer.setLogin(DEFAULT_LOGIN);
        customer.setEmail(DEFAULT_EMAIL);
    }

    @Test
    @Transactional
    public void getAllUsers() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the users
        restCustomerMockMvc.perform(get("/api/customers?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].login").value(hasItem(DEFAULT_LOGIN)))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRSTNAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LASTNAME)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].balance").value(notNullValue()));
    }

    @Test
    @Transactional
    public void getCustomer() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get the customer
        restCustomerMockMvc.perform(get("/api/customers/{id}", customer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.login").value(customer.getLogin()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRSTNAME))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LASTNAME))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.balance").value(notNullValue()));
    }

    @Test
    @Transactional
    public void getNonExistingCustomer() throws Exception {
        restCustomerMockMvc.perform(get("/api/customers/256"))
            .andExpect(status().isNotFound());
    }


    @Test
    @Transactional
    public void getAllAuthorities() throws Exception {
        restCustomerMockMvc.perform(get("/api/customers/authorities")
            .accept(TestUtil.APPLICATION_JSON_UTF8)
            .contentType(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").value(containsInAnyOrder(AuthoritiesConstants.USER, AuthoritiesConstants.ADMIN)));
    }

    @Test
    @Transactional
    public void testCustomerEquals() throws Exception {
        TestUtil.equalsVerifier(Customer.class);
        Customer customer1 = new Customer();
        customer1.setId(1L);
        Customer customer2 = new Customer();
        customer2.setId(customer1.getId());
        assertThat(customer1).isEqualTo(customer2);
        customer2.setId(2L);
        assertThat(customer1).isNotEqualTo(customer2);
        customer1.setId(null);
        assertThat(customer1).isNotEqualTo(customer2);
    }

    @Test
    public void testCustomerFromId() {
        assertThat(customerMapper.customerFromId(DEFAULT_ID).getId()).isEqualTo(DEFAULT_ID);
        assertThat(customerMapper.customerFromId(null)).isNull();
    }

    @Test
    public void testCustomerDTOtoCustomer() {
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setId(DEFAULT_ID);
        customerDTO.setLogin(DEFAULT_LOGIN);
        customerDTO.setFirstName(DEFAULT_FIRSTNAME);
        customerDTO.setLastName(DEFAULT_LASTNAME);
        customerDTO.setEmail(DEFAULT_EMAIL);
        customerDTO.setActivated(true);
        customerDTO.setCreatedBy(DEFAULT_LOGIN);
        customerDTO.setLastModifiedBy(DEFAULT_LOGIN);
        customerDTO.setAuthorities(Collections.singleton(AuthoritiesConstants.USER));

        Customer customer = customerMapper.customerDTOtoCustomer(customerDTO);
        assertThat(customer.getId()).isEqualTo(DEFAULT_ID);
        assertThat(customer.getLogin()).isEqualTo(DEFAULT_LOGIN);
        assertThat(customer.getFirstName()).isEqualTo(DEFAULT_FIRSTNAME);
        assertThat(customer.getLastName()).isEqualTo(DEFAULT_LASTNAME);
        assertThat(customer.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(customer.getActivated()).isEqualTo(true);
        assertThat(customer.getCreatedBy()).isNull();
        assertThat(customer.getCreatedDate()).isNotNull();
        assertThat(customer.getLastModifiedBy()).isNull();
        assertThat(customer.getLastModifiedDate()).isNotNull();
        assertThat(customer.getAuthorities()).extracting("name").containsExactly(AuthoritiesConstants.USER);
    }

    @Test
    public void testCustomerToCustomerDTO() {
        customer.setId(DEFAULT_ID);
        customer.setCreatedBy(DEFAULT_LOGIN);
        customer.setCreatedDate(Instant.now());
        customer.setLastModifiedBy(DEFAULT_LOGIN);
        customer.setLastModifiedDate(Instant.now());
        Set<Authority> authorities = new HashSet<>();
        Authority authority = new Authority();
        authority.setName(AuthoritiesConstants.USER);
        authorities.add(authority);
        customer.setAuthorities(authorities);

        CustomerDTO customerDTO = customerMapper.customerToCustomerDTO(customer);

        assertThat(customerDTO.getId()).isEqualTo(DEFAULT_ID);
        assertThat(customerDTO.getLogin()).isEqualTo(DEFAULT_LOGIN);
        assertThat(customerDTO.getFirstName()).isEqualTo(DEFAULT_FIRSTNAME);
        assertThat(customerDTO.getLastName()).isEqualTo(DEFAULT_LASTNAME);
        assertThat(customerDTO.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(customerDTO.isActivated()).isEqualTo(true);
        assertThat(customerDTO.getCreatedBy()).isEqualTo(DEFAULT_LOGIN);
        assertThat(customerDTO.getCreatedDate()).isEqualTo(customer.getCreatedDate());
        assertThat(customerDTO.getLastModifiedBy()).isEqualTo(DEFAULT_LOGIN);
        assertThat(customerDTO.getLastModifiedDate()).isEqualTo(customer.getLastModifiedDate());
        assertThat(customerDTO.getAuthorities()).containsExactly(AuthoritiesConstants.USER);
        assertThat(customerDTO.toString()).isNotNull();
    }

    @Test
    public void testAuthorityEquals() throws Exception {
        Authority authorityA = new Authority();
        assertThat(authorityA).isEqualTo(authorityA);
        assertThat(authorityA).isNotEqualTo(null);
        assertThat(authorityA).isNotEqualTo(new Object());
        assertThat(authorityA.hashCode()).isEqualTo(0);
        assertThat(authorityA.toString()).isNotNull();

        Authority authorityB = new Authority();
        assertThat(authorityA).isEqualTo(authorityB);

        authorityB.setName(AuthoritiesConstants.ADMIN);
        assertThat(authorityA).isNotEqualTo(authorityB);

        authorityA.setName(AuthoritiesConstants.USER);
        assertThat(authorityA).isNotEqualTo(authorityB);

        authorityB.setName(AuthoritiesConstants.USER);
        assertThat(authorityA).isEqualTo(authorityB);
        assertThat(authorityA.hashCode()).isEqualTo(authorityB.hashCode());
    }
}
