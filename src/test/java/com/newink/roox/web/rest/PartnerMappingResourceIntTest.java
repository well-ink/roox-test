package com.newink.roox.web.rest;

import com.newink.roox.TestApp;
import com.newink.roox.domain.Customer;
import com.newink.roox.domain.PartnerMapping;
import com.newink.roox.repository.PartnerMappingRepository;
import com.newink.roox.service.PartnerMappingService;
import com.newink.roox.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.newink.roox.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PartnerMappingResource REST controller.
 *
 * @see PartnerMappingResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApp.class)
public class PartnerMappingResourceIntTest {

    private static final String DEFAULT_PARTNER = "@facebook";
    private static final String UPDATED_PARTNER = "@twitter";

    private static final String DEFAULT_PARTNER_PROFILE_ID = "AAAAAAAAAA";
    private static final String UPDATED_PARTNER_PROFILE_ID = "BBBBBBBBBB";

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_MIDDLE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_MIDDLE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_IMAGE_URL = "AAAAAAAAAA";
    private static final String UPDATED_IMAGE_URL = "BBBBBBBBBB";
    public static final long NONEXISTENT_PARTNER_MAPPING_ID = 10L;

    @Autowired
    private PartnerMappingRepository partnerMappingRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private PartnerMappingService partnerMappingService;

    @Autowired
    private EntityManager em;

    private MockMvc restPartnerMappingMockMvc;

    private PartnerMapping partnerMapping;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PartnerMappingResource partnerMappingResource = new PartnerMappingResource(partnerMappingService);
        this.restPartnerMappingMockMvc = MockMvcBuilders.standaloneSetup(partnerMappingResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PartnerMapping createEntity(EntityManager em) {
        PartnerMapping partnerMapping = new PartnerMapping()
            .partner(DEFAULT_PARTNER)
            .partnerProfileId(DEFAULT_PARTNER_PROFILE_ID)
            .firstName(DEFAULT_FIRST_NAME)
            .middleName(DEFAULT_MIDDLE_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .imageUrl(DEFAULT_IMAGE_URL);
        // Add required entity
        Customer customer = CustomerResourceIntTest.createEntity(em);
        em.merge(customer);
        em.flush();
        partnerMapping.setCustomer(customer);
        return partnerMapping;
    }

    @Before
    public void initTest() {
        partnerMapping = createEntity(em);
    }

    @Test
    @Transactional
    @WithMockUser("admin")
    public void createPartnerMapping() throws Exception {
        int databaseSizeBeforeCreate = partnerMappingRepository.findAll().size();

        // Create the PartnerMapping
        restPartnerMappingMockMvc.perform(post("/api/customers/@me/partner-mappings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(partnerMapping)))
            .andExpect(status().isCreated());

        // Validate the PartnerMapping in the database
        List<PartnerMapping> partnerMappingList = partnerMappingRepository.findAll();
        assertThat(partnerMappingList).hasSize(databaseSizeBeforeCreate + 1);
        PartnerMapping testPartnerMapping = partnerMappingList.get(partnerMappingList.size() - 1);
        assertThat(testPartnerMapping.getPartnerToken()).isEqualTo(DEFAULT_PARTNER);
        assertThat(testPartnerMapping.getPartnerProfileId()).isEqualTo(DEFAULT_PARTNER_PROFILE_ID);
        assertThat(testPartnerMapping.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testPartnerMapping.getMiddleName()).isEqualTo(DEFAULT_MIDDLE_NAME);
        assertThat(testPartnerMapping.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testPartnerMapping.getImageUrl()).isEqualTo(DEFAULT_IMAGE_URL);
    }

    @Test
    @Transactional
    @WithMockUser("admin")
    public void createPartnerMappingWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = partnerMappingRepository.findAll().size();

        // Create the PartnerMapping with an existing ID
        partnerMapping.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPartnerMappingMockMvc.perform(post("/api/customers/@me/partner-mappings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(partnerMapping)))
            .andExpect(status().isBadRequest());

        // Validate the PartnerMapping in the database
        List<PartnerMapping> partnerMappingList = partnerMappingRepository.findAll();
        assertThat(partnerMappingList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    @WithMockUser("admin")
    public void checkPartnerIsRequired() throws Exception {
        int databaseSizeBeforeTest = partnerMappingRepository.findAll().size();
        // set the field null
        partnerMapping.setPartnerToken(null);

        // Create the PartnerMapping, which fails.

        restPartnerMappingMockMvc.perform(post("/api/customers/@me/partner-mappings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(partnerMapping)))
            .andExpect(status().isBadRequest());

        List<PartnerMapping> partnerMappingList = partnerMappingRepository.findAll();
        assertThat(partnerMappingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    @WithMockUser("admin")
    public void checkPartnerProfileIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = partnerMappingRepository.findAll().size();
        // set the field null
        partnerMapping.setPartnerProfileId(null);

        // Create the PartnerMapping, which fails.

        restPartnerMappingMockMvc.perform(post("/api/customers/@me/partner-mappings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(partnerMapping)))
            .andExpect(status().isBadRequest());

        List<PartnerMapping> partnerMappingList = partnerMappingRepository.findAll();
        assertThat(partnerMappingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    @WithMockUser("admin")
    public void checkFirstNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = partnerMappingRepository.findAll().size();
        // set the field null
        partnerMapping.setFirstName(null);

        // Create the PartnerMapping, which fails.

        restPartnerMappingMockMvc.perform(post("/api/customers/@me/partner-mappings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(partnerMapping)))
            .andExpect(status().isBadRequest());

        List<PartnerMapping> partnerMappingList = partnerMappingRepository.findAll();
        assertThat(partnerMappingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    @WithMockUser("admin")
    public void checkLastNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = partnerMappingRepository.findAll().size();
        // set the field null
        partnerMapping.setLastName(null);

        // Create the PartnerMapping, which fails.

        restPartnerMappingMockMvc.perform(post("/api/customers/@me/partner-mappings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(partnerMapping)))
            .andExpect(status().isBadRequest());

        List<PartnerMapping> partnerMappingList = partnerMappingRepository.findAll();
        assertThat(partnerMappingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    @WithMockUser("admin")
    public void getAllPartnerMappings() throws Exception {
        // Initialize the database
        partnerMappingRepository.saveAndFlush(partnerMapping);

        // Get all the partnerMappingList
        restPartnerMappingMockMvc.perform(get("/api/customers/@me/partner-mappings?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(partnerMapping.getId().intValue())))
            .andExpect(jsonPath("$.[*].partnerToken").value(hasItem(DEFAULT_PARTNER)))
            .andExpect(jsonPath("$.[*].partnerProfileId").value(hasItem(DEFAULT_PARTNER_PROFILE_ID)))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].middleName").value(hasItem(DEFAULT_MIDDLE_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
            .andExpect(jsonPath("$.[*].imageUrl").value(hasItem(DEFAULT_IMAGE_URL)));
    }

    @Test
    @Transactional
    public void getPartnerMapping() throws Exception {
        // Initialize the database
        partnerMappingRepository.saveAndFlush(partnerMapping);

        // Get the partnerMapping
        restPartnerMappingMockMvc.perform(get("/api/partner-mappings/{id}", partnerMapping.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(partnerMapping.getId().intValue()))
            .andExpect(jsonPath("$.partnerToken").value(DEFAULT_PARTNER))
            .andExpect(jsonPath("$.partnerProfileId").value(DEFAULT_PARTNER_PROFILE_ID))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME))
            .andExpect(jsonPath("$.middleName").value(DEFAULT_MIDDLE_NAME))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME))
            .andExpect(jsonPath("$.imageUrl").value(DEFAULT_IMAGE_URL));
    }

    @Test
    @Transactional
    public void getNonExistingPartnerMapping() throws Exception {
        // Get the partnerMapping
        restPartnerMappingMockMvc.perform(get("/api/partner-mappings/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithMockUser("admin")
    public void updatePartnerMapping() throws Exception {
        // Initialize the database
        partnerMappingRepository.saveAndFlush(partnerMapping);
        int databaseSizeBeforeUpdate = partnerMappingRepository.findAll().size();

        // Update the partnerMapping
        PartnerMapping updatedPartnerMapping = partnerMappingRepository.findOne(partnerMapping.getId());
        // Disconnect from session so that the updates on updatedPartnerMapping are not directly saved in db
        em.detach(updatedPartnerMapping);
        updatedPartnerMapping
            .partner(UPDATED_PARTNER)
            .partnerProfileId(UPDATED_PARTNER_PROFILE_ID)
            .firstName(UPDATED_FIRST_NAME)
            .middleName(UPDATED_MIDDLE_NAME)
            .lastName(UPDATED_LAST_NAME)
            .imageUrl(UPDATED_IMAGE_URL);

        restPartnerMappingMockMvc.perform(put("/api/customers/@me/partner-mappings/")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPartnerMapping)))
            .andExpect(status().isOk());

        // Validate the PartnerMapping in the database
        List<PartnerMapping> partnerMappingList = partnerMappingRepository.findAll();
        assertThat(partnerMappingList).hasSize(databaseSizeBeforeUpdate);
        PartnerMapping testPartnerMapping = partnerMappingList.get(partnerMappingList.size() - 1);
        assertThat(testPartnerMapping.getPartnerToken()).isEqualTo(UPDATED_PARTNER);
        assertThat(testPartnerMapping.getPartnerProfileId()).isEqualTo(UPDATED_PARTNER_PROFILE_ID);
        assertThat(testPartnerMapping.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testPartnerMapping.getMiddleName()).isEqualTo(UPDATED_MIDDLE_NAME);
        assertThat(testPartnerMapping.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testPartnerMapping.getImageUrl()).isEqualTo(UPDATED_IMAGE_URL);
    }

    @Test
    @Transactional
    @WithMockUser("admin")
    public void updateNonExistingPartnerMapping() throws Exception {

        int databaseSizeBeforeUpdate = partnerMappingRepository.findAll().size();

        // Create the PartnerMapping

        restPartnerMappingMockMvc.perform(put("/api/customers/@me/partner-mappings/")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(partnerMapping)))
            .andExpect(status().isCreated());

        // Validate the PartnerMapping in the database
        List<PartnerMapping> partnerMappingList = partnerMappingRepository.findAll();
        assertThat(partnerMappingList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePartnerMapping() throws Exception {
        // Initialize the database
        partnerMappingRepository.saveAndFlush(partnerMapping);
        int databaseSizeBeforeDelete = partnerMappingRepository.findAll().size();

        // Get the partnerMapping
        restPartnerMappingMockMvc.perform(delete("/api/partner-mappings/{id}", partnerMapping.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PartnerMapping> partnerMappingList = partnerMappingRepository.findAll();
        assertThat(partnerMappingList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PartnerMapping.class);
        PartnerMapping partnerMapping1 = new PartnerMapping();
        partnerMapping1.setId(1L);
        PartnerMapping partnerMapping2 = new PartnerMapping();
        partnerMapping2.setId(partnerMapping1.getId());
        assertThat(partnerMapping1).isEqualTo(partnerMapping2);
        partnerMapping2.setId(2L);
        assertThat(partnerMapping1).isNotEqualTo(partnerMapping2);
        partnerMapping1.setId(null);
        assertThat(partnerMapping1).isNotEqualTo(partnerMapping2);
    }
}
